# Assignment 3
​
### Table of contents
* [Introduction](#introduction)
* [Hibernate and Web API](#hibernate-and-web-api)
* [Deployment](#deployment)
* [Where to access/installations](#where-to-acces/installation)
* [Authors](#authors)
​
​
## Introduction
​

This project is our submission to assignment 3 of the Noroff Accelerate Fullstack program.
The task was given to us by Livinus Obiora Nweke, lecturer at Noroff University College, Full-Stack Java Development course.
The goal of this project is to create a PostgresSQL database using Hibernate and access it through a deployed Web API.
Our database consist of movies, franchises and characters(parts) as requested by the assignment.


## Hibernate and Web API:
​
### Creating Repository, Models and Service classes
​
We started building our project by creating models for each respective entity in the database; Part, Movie and Franchise. These classes contains getters 
and setter for the given component, so that we can for example set a movies title or who plays in the movie later on. We expanded on this by creating a 
repository class for each of them, serving as a way to represent an entity from the datebase. We also created Service interfaces which implements a CRUDInterface,
to help structure our Service implementations. These implementations allow us to interact with the components in the database as a whole; to for example search by 
a specific movie with a given integer.
​

### Creating Controller, DTO and Mapper classes
​
Once we had the classes to implement and interact with the database finishes, we started working on implementing our project with Spring Web. 
To achieve this, we created a DTO and a mapper class for each of the components; these two work in tandem to map our entities together.
The DTO is a Data Transferable Object; a simpler way to transfer information without annotation and method bloat. The mapper interface is then used to map these
DTO references together, to create the connections within the database so that we might for example find out which entities are related to what.
​
Lastly, we created controller classes for each of our entity types. These controller classes handle requests from the mapper(and user) and fetches and modifies the database as
told. We used @RestControll which greatly reduces redundant code and implemented the methods specified by the assignment:
​
* Get all entities of a type
* Get an entity by id
* Add a new entity to the database
* Update an entity
* Delete an entity
​
Furthermore, we added specific methods for some of our controllers:
​
* Get all parts in a given movie
* Update a movie with a new set of parts
* Get all movies in a franchise
* Update movies within a franchise
​
A challenge to overcome was of course to make sure that the related entities were changed accordingly when its linked entity is updated (such as the actor being removed
from a movie also has this reflected on their table) but this was overcome by the mapping structures and making sure to update their relevant attribute upon changing their
linked entity.
​

## Deployment
​
### Testing with Swagger
​
Once our database had the necessary components to work, we started working on the documentation and it's implementation through Swagger. This mostly consisted of adding appropriate
annotations in our controllers and make sure that what was being processed was aptly reflected in the feedback text on Swagger. Furthermore, we did numerous tests to make sure our 
code worked properly through Swagger and it provided great insight to fixing bugs in our code.
​
​
### Deployment and configuration with Docker and Heroku


Lastly we configured Docker to allow us to create a Docker Artefact from our project. This artefact creates containers which is excellent for containing and packaging our code.
Furthermore, it allows us to create Docker images; these images provides information on how the software is ran, what it requires in terms of resources and time, as well as
other useful instructions. It allows us to run the program in multiple different environments as the code is packaged neatly and remains modular.
​
We deployed our project on Heroku. This is a service that allows us to build and run our application in a virtual container online. Currently the Heroku application is not working.
​

​
### DIRECTORY
​
```
Assignment 3
| 
| .gitignore
| build.gradle
| Dockerfile
| gradlew
| gradlew.bat
| README.md
| settings.gradle
|___ src
|      | 
|      |___ main
|      |     |___ java
|      |     |    |___ com.example.assignment3
|      |     |         |___ controllers
|      |     |         |    | FranchiseController
|      |     |         |    | MovieController
|      |     |         |    | PartController
|      |     |         |
|      |     |         |___ exceptions
|      |     |         |    | FranchiseNotFoundException
|      |     |         |    | MovieNotFoundException
|      |     |         |    | PartNotFoundExceptino
|      |     |         |    
|      |     |         |___ mappers
|      |     |         |    | FranchiseMapper
|      |     |         |    | MovieMapper
|      |     |         |    | PartMapper
|      |     |         |    
|      |     |         |___ models
|      |     |         |    |___ dtos
|      |     |         |    |    | FranchiseDTO
|      |     |         |    |    | MovieDTO
|      |     |         |    |    | PartDTO
|      |     |         |    | 
|      |     |         |    | Franchise
|      |     |         |    | Movie
|      |     |         |    | Part
|      |     |         | 
|      |     |         |___ repositories
|      |     |         |    | FranchiseRepository
|      |     |         |    | MovieRepository
|      |     |         |    | PartRepository
|      |     |         |    
|      |     |         |___ services
|      |     |         |    |___ franchise
|      |     |         |    |    | FranchiseService
|      |     |         |    |    | FranchiseServiceImplements
|      |     |         |    |
|      |     |         |    |___ movie
|      |     |         |    |    | MovieService
|      |     |         |    |    | MovieServiceImplements
|      |     |         |    |
|      |     |         |    |___ part
|      |     |         |    |    | PartService
|      |     |         |    |    | PartServiceImplements
|      |     |         |    |
|      |     |         |    | CrudService
|      |     |         |   
|      |     |         |___ util
|      |     |         |    | ApiErrorResponse
|      |     |         |  
|      |     |         | Assignment3Application
|      |     |         | ProdDatabaseConfig
|      |     |  
|      |     |___ resources
|      |     |    | application.properties
|      |     |    | data.sql
|      |     |  
|      |
|      |___ test
|      |     |___ com.example.assignment3
|      |     |    |Assignment3ApplicationTests
```
​

​
## Where to access/installations

Create an SSH key and pull the project from the git repository.

You can always access the project through Heroku on: https://blooming-depths-80754.herokuapp.com/
​
​

​
## Project status
Finished.
​

​
## License
This project is open-source. You are free to use any of the code in your own projects, as long as the work is credited.
​

​
## Authors and acknowledgment
**Code authors:**
- [Marius Eriksen](https://gitlab.com/marius.eriksen13)
- [Linnea Johansen](https://gitlab.com/LinneaJohansen)
- [Sondre Melhus](https://gitlab.com/SondreMelhus)
​

**Assignment given by:** 

Livinus Obiora Nweke, Lecturer at Noroff University College
