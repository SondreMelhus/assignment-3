package com.example.assignment3.models;

import javax.persistence.*;
import java.util.Set;

//Class that is used to represent a Part entity
//NB: Character has been changed to Part. This was to avoid any conflict with Character wrapper
@Entity
public class Part {

    //Variable used to store the integer primary key for Part entity
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //Variable used to store the name of a Part entity
    @Column(length = 50, nullable = false)
    private String name;

    //Variable used to store the alias of a Part entity
    @Column(length = 50)
    private String alias;

    //Variable used to store a Part entities gender
    @Column(length = 10)
    private String gender;

    //Variable used to store the pictureURL of a Part entity
    @Column(length = 255)
    private String pictureUrl;

    //Variable used to store a Set of all the movies that are a part of the franchise
    @ManyToMany (mappedBy = "parts")
    private Set<Movie> movies;



    //Getter and setter methods

    //Id
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    //Name
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    //Alias
    public String getAlias() {return alias;}
    public void setAlias(String alias) {this.alias = alias;}

    //Gender
    public String getGender() {return gender;}
    public void setGender(String gender) {this.gender = gender;}

    //PictureURL
    public String getpictureUrl() {return pictureUrl;}
    public void setpictureUrl(String pictureUrl) {this.pictureUrl = pictureUrl;}

    //Movies
    public Set<Movie> getMovies(){ return movies;}
    public void setMovies(Set<Movie> parts) {this.movies = movies;}

}
