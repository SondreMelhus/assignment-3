package com.example.assignment3.models.dtos;

import lombok.Data;

import java.util.Set;


// Data Transfer Object (DTO) for Part domain class
@Data
public class MovieDTO {

    private int id;
    private Set<Integer> parts;
    private int franchise;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureUrl;
    private String trailerUrl;
}
