package com.example.assignment3.models.dtos;


import lombok.Data;

import java.util.Set;

// Data Transfer Object (DTO) for Part domain class
@Data
public class PartDTO {

    private int id;
    private String name;
    private String alias;
    private String gender;
    private String pictureUrl;
    private Set<Integer> movies;
}
