package com.example.assignment3.models.dtos;

import lombok.Data;

import java.util.Set;

// Data Transfer Object (DTO) for Franchise domain class
@Data
public class FranchiseDTO {

    private int id;
    private String name;
    private String description;
    private Set<Integer> movies;
}
