package com.example.assignment3.models;

import javax.persistence.*;
import java.util.Set;

//Class that is used to represent a Part entity
@Entity
public class Movie {

    //Variable used to store the integer primary key for Movie entity
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //Variable used to store the title of a Movie entity
    @Column(length = 50, nullable = false)
    private String title;

    //Variable used to store the genre of a Movie entity
    @Column(length = 50)
    private String genre;

    //Variable used to store the release year of a Movie entity
    @Column(length = 4, nullable = false)
    private int releaseYear;

    //Variable used to store the director of a Movie entity
    @Column(length = 50, nullable = false)
    private String director;

    //Variable used to store the pictureURL of a Movie entity
    @Column(length = 200)
    private String pictureUrl;

    //Variable used to store the trailerURL of a Movie entity
    @Column(length = 200)
    private String trailerUrl;

    //Set used to store all the parts of a Movie entity
    @ManyToMany
    @JoinTable(
            name = "part_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "part_id")}
    )
    private Set<Part> parts;

    //Variable used to store what Franchise a Movie entity belongs to
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;



    //Getters and setters

    //Id
    public int getId() {return id;}
    public void setId(int id) {this.id = id;}

    //Title
    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}

    //Genre
    public String getGenre() {return genre;}
    public void setGenre(String genre){this.genre = genre;}

    //Release year
    public int getReleaseYear() {return releaseYear;}
    public void setReleaseYear(int releaseYear){this.releaseYear = releaseYear;}

    //Director
    public String getDirector(){return director;}
    public void setDirector(String director){this.director = director;}

    //Picture URL
    public String getpictureUrl(){return pictureUrl;}
    public void setpictureUrl(String pictureUrl){this.pictureUrl = pictureUrl;}

    //Trailer URL
    public String gettrailerUrl(){return trailerUrl;}
    public void settrailerUrl(String trailerUrl){this.trailerUrl = trailerUrl;}

    //Franchise
    public Franchise getFranchise() {return franchise;}
    public void setFranchise(Franchise franchise){this.franchise = franchise;}

    //Parts
    public Set<Part> getParts(){ return parts;}
    public void setParts(Set<Part> parts) {this.parts = parts;}
}
