package com.example.assignment3.models;

import javax.persistence.*;
import java.util.Set;

//Class that is used to represent a Franchise entity
@Entity
public class Franchise {

    //Variable used to store the integer primary key for Franchise entity
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //Variable used to store the name of a Franchise entity
    @Column(length = 50, nullable = false)
    private String name;

    //Variable used to store the description of a Franchise entity
    @Column()
    private String description;

    //Variable used to store a set of all the Movies that are a part of the Franchise entity
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;



    //Getters and setters

    //Id
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    //Name
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //Description
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    //Movies
    public Set<Movie> getMovies() {
        return movies;
    }
    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}


