package com.example.assignment3.mappers;


import com.example.assignment3.models.Movie;
import com.example.assignment3.models.Part;
import com.example.assignment3.models.dtos.PartDTO;
import com.example.assignment3.services.movie.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;


import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;


//Mapper class used to map Part to PartDTO and IDs
@Mapper(componentModel = "spring")
public abstract class PartMapper {

    @Autowired
    protected MovieService movieService;

    //Part --> PartDTO
    //Use a Part entity to get a PartDTO object
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract PartDTO partToPartDto(Part part);

    //PartDTO --> Part
    //Use a PartDTO object to get a Part entity
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdToMovies")
    public abstract Part partDtoToPart(PartDTO part);

    //Collection<Part> -> Collection<PartDTO>
    //Use a Collection of Parts to get a Collection of PartDTOs
    public abstract Collection<PartDTO> partToPartDTO(Collection<Part> parts);


    //Custom mappings
    //Map movies to IDs
    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }


    //Map movieId to movie
    @Named("movieIdToMovie")
    Movie mapIdToMovie(int id) {
        return movieService.findById(id);
    }




    //Map IDs to movies
    @Named("movieIdToMovies")
    Set<Movie> mapIdsToMovies(Set<Integer> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }

}
