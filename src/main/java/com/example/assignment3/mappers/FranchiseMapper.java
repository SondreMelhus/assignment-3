package com.example.assignment3.mappers;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.Part;
import com.example.assignment3.models.dtos.FranchiseDTO;
import com.example.assignment3.services.franchise.FranchiseService;
import com.example.assignment3.services.movie.MovieService;
import com.example.assignment3.services.part.PartService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

//Mapper class used to map Franchise to FranchiseDTO and IDs
@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;


    //Franchise --> Franchise DTO
    //Use a Franchise entity to get a FranchiseDTO object
    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    //FranchiseDTO --> Franchise
    //Use a FranchiseDTO object to get a Franchise entity
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchise);

    //Collection of Franchises --> Collection of FranchiseDTO
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchise);



    /// Custom mappings ///
    //Map movies to ids
    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    //Map ids to movies
    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Integer> id) {
        return id.stream()
                .map( i -> movieService.findById(i))
                .collect(Collectors.toSet());
    }
}
