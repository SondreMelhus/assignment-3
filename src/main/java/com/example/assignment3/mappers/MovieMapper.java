package com.example.assignment3.mappers;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.Part;
import com.example.assignment3.models.dtos.MovieDTO;
import com.example.assignment3.services.franchise.FranchiseService;
import com.example.assignment3.services.movie.MovieService;
import com.example.assignment3.services.part.PartService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.relational.core.sql.In;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

//Mapper class used to map Movies to MovieDTO and IDs
@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected FranchiseService franchiseService;
    @Autowired
    protected PartService partService;

    //Movie --> MovieDTO
    //Use a Movie entity to get a MovieDTO object
    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "parts", source = "parts", qualifiedByName = "partsToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    //MovieDTO --> Movie
    //Use a MovieDTO to get a Movie object
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "parts", source = "parts", qualifiedByName = "partIdToPart")
    public abstract Movie movieDtoToMovie(MovieDTO movie);

    //Collection of Movies --> Collection of MovieDTO
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movie);


    //Custom mappings
    //Maps id to franchise
    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(int id) {
        return franchiseService.findById(id);
    }

    //Map parts to ids
    @Named("partsToIds")
    Set<Integer> mapPartsToIds(Set<Part> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }

    //Map partId to part
    @Named("partIdToPart")
    Part mapIdToPart(int id) {
        return partService.findById(id);
    }

}
