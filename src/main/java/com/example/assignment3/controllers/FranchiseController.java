package com.example.assignment3.controllers;

import com.example.assignment3.mappers.FranchiseMapper;
import com.example.assignment3.mappers.MovieMapper;
import com.example.assignment3.mappers.PartMapper;
import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.dtos.FranchiseDTO;
import com.example.assignment3.services.franchise.FranchiseService;
import com.example.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final PartMapper partMapper;
    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, PartMapper partMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.partMapper = partMapper;
    }

    //TODO:**
    // - Implement methods
    // - Add Swagger documentation

    //findAll
    @Operation(summary = "Find all franchises")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchises successfully found",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "No franchises found",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<FranchiseDTO>> getAll() {
        Collection<FranchiseDTO> franchises = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(franchises);
    }

    //findById
    @Operation(summary = "Get franchise with id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Franchise has been found",
                    content = @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))),
            @ApiResponse(responseCode = "404",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("{id}")
    public ResponseEntity<FranchiseDTO> getById(@PathVariable int id) {
        FranchiseDTO franchise = franchiseMapper.franchiseToFranchiseDto(
                franchiseService.findById(id)
        );

        return ResponseEntity.ok(franchise);
    }

    //Get all characters from a franchise
    @Operation(summary = "find parts from franchise with ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise parts has successfully been found",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("/getAllPartsFromFranchise/{id}")
    public ResponseEntity getAllCharactersInFranchise(@PathVariable int id) {
        if (!franchiseService.exists(id)) {
            ResponseEntity.badRequest().build();
        }
        Franchise franchise = franchiseService.findById(id);

        return ResponseEntity.ok(franchiseService.getAllPartsInFranchise(franchise).stream().map(part -> partMapper.partToPartDto(part)));
    }

    //Get all movies in a franchise
    @Operation(summary = "Find movies from franchise with ID")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise movies has successfully been found",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @GetMapping("/getAllMoviesInFranchise/{id}")
    public ResponseEntity getAllMoviesInFranchise(@PathVariable int id){
        if (!franchiseService.exists(id)) {
            ResponseEntity.badRequest().build();
        }
        Franchise franchise = franchiseService.findById(id);
        Collection<Movie> movies = franchiseService.getAllMoviesInFranchise(franchise);
        return ResponseEntity.ok(movies.stream().map(movie -> movieMapper.movieToMovieDto(movie)));
    }

    //add
    @Operation(summary = "Add new franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Franchise franchise) {
        Franchise franch = franchiseService.add(franchise);
        URI location = URI.create("franchises/" + franch.getId());
        return ResponseEntity.created(location).build();
    }

    //Update
    @Operation(summary = "Update franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id) {
        if (id != franchiseDTO.getId())
            return ResponseEntity.badRequest().build();
        franchiseService.update(
                franchiseMapper.franchiseDtoToFranchise(franchiseDTO));
        return ResponseEntity.noContent().build();
    }

    //Update movies in a franchise
    @Operation(summary = "Update movies in franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise has been successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("/update{id}")
    public ResponseEntity updateMoviesInFranchise (@PathVariable int id, @RequestBody Collection<Integer> movieIds) {
        if(!franchiseService.exists(id)) {
            return ResponseEntity.badRequest().build();
        }
        franchiseService.updateMoviesInFranchise(franchiseService.findById(id), movieIds);
        return ResponseEntity.noContent().build();
    }

    //deleteById
    @Operation(summary = "Delete a franchise by id")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity deleteById(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    //delete
    @Operation(summary = "Delete franchise with RequestBody")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping()
    public ResponseEntity delete(@RequestBody Franchise franchise) {
        franchiseService.delete(franchise);
        return ResponseEntity.noContent().build();
    }
}

