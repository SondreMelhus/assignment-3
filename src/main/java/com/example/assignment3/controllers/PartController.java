
package com.example.assignment3.controllers;

import com.example.assignment3.mappers.PartMapper;
import com.example.assignment3.models.Part;
import com.example.assignment3.models.dtos.PartDTO;
import com.example.assignment3.services.part.PartService;
import com.example.assignment3.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/parts")
public class PartController {

    private final PartService partService;
    private final PartMapper partMapper;
    

    public PartController(PartService partService, PartMapper partMapper) {
        this.partService = partService;
        this.partMapper = partMapper;
    }


    ///getAll///
    //Swagger documentation getAll
    @Operation(summary = "Get all parts")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = PartDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Part does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    //getAll method
    @GetMapping // GET: localhost:8081/api/v1/parts
    public ResponseEntity getAll() {
        Collection<PartDTO> parts = partMapper.partToPartDTO(
                partService.findAll()
        );
        return ResponseEntity.ok(parts);
    }


    ///getById///
    //Swagger documentation getById
    @Operation(summary = "Get a part by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = PartDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Part does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })


    //getById method
    @GetMapping("{id}") // GET: localhost:8081/api/v1/parts/1
    public ResponseEntity getById(@PathVariable int id) {
       PartDTO part = partMapper.partToPartDto(
               partService.findById(id)
       );
        return ResponseEntity.ok(part);
    }

    ///add///
    //Swagger documentation add
    @Operation(summary = "Add a new part")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Part successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
    })

    //add method
    @PostMapping // POST: localhost:8081/api/v1/parts
    public ResponseEntity add(@RequestBody Part part) {
        Part par = partService.add(part);
        URI location = URI.create("parts/" + par);
        return ResponseEntity.created(location).build();
    }

    ///update///
    //Swagger documentation update
    @Operation(summary = "Updates a part")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Part successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Student not found with supplied ID",
                    content = @Content)
    })

    //update method
    @PutMapping("{id}") // PUT: localhost:8081/api/v1/part/1
    public ResponseEntity update(@RequestBody PartDTO partDTO, @PathVariable int id) {
        // Validates if body is correct
        if(id != partDTO.getId())
            return ResponseEntity.badRequest().build();
        partService.update(
                partMapper.partDtoToPart(partDTO)
        );
        return ResponseEntity.noContent().build();
    }


    //delete
    //Swagger documentation update
    @Operation(summary = "Delete a part")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Part successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "Part not found with supplied Part",
                    content = @Content)
    })

    //delete method
    @DeleteMapping // DELETE: localhost:8081/api/v1/parts
    public ResponseEntity delete(@RequestBody Part part) {
        partService.delete(part);
        return ResponseEntity.noContent().build();
    }

    //deleteById
    //Swagger documentation deleteById
    @Operation(summary = "Delete a part by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    array = @ArraySchema(schema = @Schema(implementation = PartDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "Part does not exist with supplied ID",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    //deleteById method
    @DeleteMapping("{id}") // DELETE: localhost:8081/api/v1/parts/1
    public ResponseEntity deleteById(@PathVariable int id) {
        partService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
