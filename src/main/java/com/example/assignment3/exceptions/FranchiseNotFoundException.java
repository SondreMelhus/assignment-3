package com.example.assignment3.exceptions;

public class FranchiseNotFoundException extends RuntimeException {
    public FranchiseNotFoundException(String message) {
        super(message);
    }

    public FranchiseNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FranchiseNotFoundException(Throwable cause) {
        super(cause);
    }

    public FranchiseNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
