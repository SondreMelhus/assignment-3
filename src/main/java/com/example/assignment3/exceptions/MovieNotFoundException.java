package com.example.assignment3.exceptions;

public class MovieNotFoundException extends RuntimeException {
    public MovieNotFoundException(int id) {
        super("Movie with ID "+id+" does not exist");
    }
}
