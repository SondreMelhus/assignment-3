package com.example.assignment3.services.franchise;

import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.Part;
import com.example.assignment3.services.CrudService;

import java.util.Collection;

//Service for the Franchise domain class.
//Providing basic CRUD functionality through CrudService and any extended functionality.
public interface FranchiseService extends CrudService <Franchise, Integer> {
    Collection<Part> getAllPartsInFranchise(Franchise franchise);
    Collection<Movie> getAllMoviesInFranchise(Franchise franchise);
    Franchise updateMoviesInFranchise(Franchise franchise, Collection<Integer> movieIds);
}
