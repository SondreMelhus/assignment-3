package com.example.assignment3.services.franchise;


import com.example.assignment3.exceptions.FranchiseNotFoundException;
import com.example.assignment3.models.Franchise;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.Part;
import com.example.assignment3.repositories.FranchiseRepository;
import com.example.assignment3.repositories.MovieRepository;
import com.example.assignment3.repositories.PartRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


//Implementation of FranchiseService.
//Uses Franchise repository to interact with data store.
@Service
public class FranchiseServiceImplmements implements FranchiseService{

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;
    private final PartRepository partRepository;
    private final Logger logger = LoggerFactory.getLogger(FranchiseServiceImplmements.class);

    public FranchiseServiceImplmements(FranchiseRepository franchiseRepository, MovieRepository movieRepository, PartRepository partRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
        this.partRepository = partRepository;
    }



    //Find a Franchise entity by id
    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new FranchiseNotFoundException("No franchise with given id"));
    }


    //Find all the Franchise entities
    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }


    //Add a new Franchise entity
    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }


    //Updates a Franchise entity
    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }


    //Delete a Franchise entity by id
    @Override
    public void deleteById(Integer id) {
        franchiseRepository.deleteById(id);
    }


    //Delete a Franchise entity
    @Override
    public void delete(Franchise entity) {
        franchiseRepository.delete(entity);
    }


    //Check if a Franchise entity exists
    @Override
    public boolean exists(Integer id) {
        return franchiseRepository.existsById(id);
    }

    @Override
    public Collection<Part> getAllPartsInFranchise(Franchise franchise) {
        Set<Part> parts = new HashSet<Part>();
        franchiseRepository.findById(franchise.getId()).get().getMovies().forEach(movie -> movie.getParts().forEach(part -> parts.add(part)));
        return parts;
    }

    @Override
    public Collection<Movie> getAllMoviesInFranchise(Franchise franchise) {
        Collection<Movie> moviesInFranchise = franchise.getMovies();
        return moviesInFranchise;
    }

    public Franchise updateMoviesInFranchise (Franchise franchise, Collection<Integer> ids){
        franchise.getMovies().forEach(c -> c.setFranchise(null));

        //Adding the part to the movie
        ids.stream().forEach(p -> movieRepository.findById(p).get().setFranchise(franchise));

        return franchiseRepository.save(franchise);
    }
}
