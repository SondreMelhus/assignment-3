package com.example.assignment3.services.movie;


import com.example.assignment3.exceptions.MovieNotFoundException;
import com.example.assignment3.models.Movie;
import com.example.assignment3.models.Part;
import com.example.assignment3.repositories.MovieRepository;
import com.example.assignment3.repositories.PartRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;


//Implementation of MovieService.
//Uses Movie repository to interact with data store.
@Service
public class MovieServiceImplements implements MovieService{

    private final MovieRepository movieRepository;
    private final PartRepository partRepository;

    public MovieServiceImplements(MovieRepository movieRepository, PartRepository partRepository) {
        this.movieRepository = movieRepository;
        this.partRepository = partRepository;
    }



    //Find a Movie entity by id
    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id)
                .orElseThrow(() -> new MovieNotFoundException(id));
    }

    //Find all the Movie entities
    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    //Add a new Movie entity
    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    //Updates a Movie entity
    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    //Delete a Movie entity by id
    @Override
    public void deleteById(Integer id) {movieRepository.deleteById(id);}

    //Delete a Movie entity
    @Override
    public void delete(Movie entity) {movieRepository.delete(entity);}

    //Check if a Movie entity exists
    @Override
    public boolean exists(Integer id) {return movieRepository.existsById(id);}

    public Movie updatePartsInMovie (Movie movie, Collection<Integer> ids){
        movie.getParts().forEach(c -> c.setMovies(null));
        Set<Part> parts = new HashSet<Part>();

        //Adding the part to the movie
        ids.stream().forEach(p -> parts.add(partRepository.findById(p).get()));

        movie.setParts(parts);
        return movieRepository.save(movie);
    }
}
