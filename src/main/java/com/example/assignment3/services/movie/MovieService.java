package com.example.assignment3.services.movie;

import com.example.assignment3.models.Movie;
import com.example.assignment3.services.CrudService;

import java.util.Collection;


//Service for the Movie domain class.
//Providing basic CRUD functionality through CrudService and any extended functionality.
public interface MovieService extends CrudService <Movie, Integer> {
    Movie updatePartsInMovie(Movie movie, Collection<Integer> ids);
}
