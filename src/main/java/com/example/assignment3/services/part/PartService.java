package com.example.assignment3.services.part;

import com.example.assignment3.models.Part;
import com.example.assignment3.services.CrudService;

//Service for the Part domain class.
//Providing basic CRUD functionality through CrudService and any extended functionality.
public interface PartService extends CrudService <Part, Integer> {
}
