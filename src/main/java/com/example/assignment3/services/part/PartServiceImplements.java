package com.example.assignment3.services.part;

import com.example.assignment3.exceptions.PartNotFoundException;
import com.example.assignment3.models.Part;
import com.example.assignment3.repositories.PartRepository;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;


//Implementation of PartService.
//Uses Part repository to interact with data store.
@Service
public class PartServiceImplements implements PartService {

    private final PartRepository partRepository;
    private final Logger logger = LoggerFactory.getLogger(PartServiceImplements.class);

    public PartServiceImplements(PartRepository partRepository) {
        this.partRepository = partRepository;
    }



    //Find a Part entity by id
    @Override
    public Part findById(Integer id) {
        return partRepository.findById(id)
                .orElseThrow(() -> new PartNotFoundException(id));
    }

    //Find all the Part entities
    @Override
    public Collection<Part> findAll() {
        return partRepository.findAll();
    }

    //Add a new Part entity
    @Override
    public Part add(Part entity) {
        return partRepository.save(entity);
    }

    //Updates a Part entity
    @Override
    public Part update(Part entity) {
        return partRepository.save(entity);
    }

    //Delete a Part entity by id
    @Override
    public void deleteById(Integer id) {
        partRepository.deleteById(id);
    }

    //Delete a Part entity
    @Override
    public void delete(Part entity) {
        partRepository.delete(entity);
    }

    //Checks if Part entity exists
    @Override
    public boolean exists(Integer id) {
        return partRepository.existsById(id);
    }
}
