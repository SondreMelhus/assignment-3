package com.example.assignment3.repositories;


import com.example.assignment3.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


//Repository (DAO) for the Movie domain class
@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

}
