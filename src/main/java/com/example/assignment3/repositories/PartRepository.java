package com.example.assignment3.repositories;


import com.example.assignment3.models.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


//Repository (DAO) for the Part domain class
@Repository
public interface PartRepository extends JpaRepository<Part, Integer> {
}
