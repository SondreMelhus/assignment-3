package com.example.assignment3.repositories;


import com.example.assignment3.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


//Repository (DAO) for the Franchise domain class
@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
}
