-- Franchise
INSERT INTO franchise ("name", description) VALUES ('Merge Wars', 'Stories from a galaxy far far away...');
INSERT INTO franchise ("name", description) VALUES ('Keeping up with the normies', 'Normaly pretty normal');
INSERT INTO franchise ("name", description) VALUES ('The merge', 'A story of conflicts and missery');

-- Movie

--Franchise 1
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('A new merge', 'Psychological horror', 2018, 'Livinus Obiora Nweke', 'Placeholder','Placeholder', 1);
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('The force push', 'Splatter horror', 2021, 'Livinus Obiora Nweke', 'Placeholder','Placeholder', 1);
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('Return of the merge', 'Cosmic horror', 2024, 'Livinus Obiora Nweke', 'Placeholder','Placeholder', 1);

--Franchise 2
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('Im Watching you', 'Comedy', 2013, 'Tim Cardashcamian', 'Placeholder','Placeholder', 2);
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('Meet the Cardashcamians', 'Clown fiesta', 2014, 'Jim Cardashcamian', 'Placeholder','Placeholder', 2);
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('Blame it on the merge', 'Horror comdey', 2015, 'Pim Cardashcamian', 'Placeholder','Placeholder', 2);

--Franchise 3
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('The commit message', 'Romantic horror', 2013, 'Thick Cashavetes', 'Placeholder','Placeholder', 3);
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('The great Gitsby', 'Romantic', 2017, 'Leonardo DiPiastro', 'Placeholder','Placeholder', 3);
INSERT INTO movie ("title", genre, release_year, director, picture_url, trailer_url, franchise_id) VALUES ('Merge and other drugs', 'Romnatic comdey', 2015, 'Marea Van Gogh', 'Placeholder','Placeholder', 3);

-- Part
INSERT INTO part ("name", alias, gender, picture_url) VALUES ('Merge Hamill', 'Luke Mergewalker', 'Male', 'https://sciencefiction.com/wp-content/uploads/2017/12/lukeskywalkerfeaturedthumb.jpg');
INSERT INTO part ("name", alias, gender, picture_url) VALUES ('Kim Cardashcamian', 'Kim Salabim', 'Female', 'https://i.pinimg.com/736x/4f/3c/c7/4f3cc7897f9dc45fb81d4ca00df5acc6.jpg');
INSERT INTO part ("name", alias, gender, picture_url) VALUES ('Ryan Gooseling', 'Mr Ken', 'Male', 'https://phantom-marca.unidadeditorial.es/951a1ca91903fabfecac0ac837ee8a5d/resize/1320/f/jpg/assets/multimedia/imagenes/2022/06/19/16556554784469.jpg');

-- part_movie

-- Part 1
INSERT INTO part_movie (part_id, movie_id) VALUES (1,1);
INSERT INTO part_movie (part_id, movie_id) VALUES (1,2);
INSERT INTO part_movie (part_id, movie_id) VALUES (1,3);

-- Part 2
INSERT INTO part_movie (part_id, movie_id) VALUES (2,4);
INSERT INTO part_movie (part_id, movie_id) VALUES (2,5);
INSERT INTO part_movie (part_id, movie_id) VALUES (2,6);

-- Part 3
INSERT INTO part_movie (part_id, movie_id) VALUES (3,7);
INSERT INTO part_movie (part_id, movie_id) VALUES (3,8);
INSERT INTO part_movie (part_id, movie_id) VALUES (3,9);